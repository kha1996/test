import java.net.URL;
import java.time.Duration;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;

public class test {
	static AppiumDriver<MobileElement> driver;

	public static void main(String[] args) throws Exception {
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
		caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, "9.0");
		caps.setCapability(MobileCapabilityType.DEVICE_NAME, "Galaxy S9");
		// UDID in my case 192.168.0.19:5555
		caps.setCapability(MobileCapabilityType.UDID, "192.168.0.19:5555");
		caps.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 60);
		caps.setCapability(MobileCapabilityType.APP, "C:\\Users\\User\\Desktop\\youtility-test.apk");
		// caps.setCapability(MobileCapabilityType.BROWSER_NAME, "Chrome");
		URL url = new URL("http://127.0.0.1:4723/wd/hub");
		driver = new AppiumDriver<MobileElement>(url, caps);
		Thread.sleep(6000);
		System.out.println(driver.findElement(By.id("com.youtility.test:id/tvBottom")).getText());
		WebElement Panel = driver.findElement(By.id("com.youtility.test:id/pager"));

		SwipeScreen(Panel, "toRight");
		SwipeScreen(Panel, "toLeft");
		SwipeScreen(Panel, "toRight");
		SwipeScreen(Panel, "toRight");
	}

	public static void SwipeScreen(WebElement Panel, String side) throws InterruptedException {
		Dimension dimension = Panel.getSize();
		int Anchor = Panel.getSize().getHeight();
		Double initial = 0.9;
		Double finall = 0.1;
		switch (side) {
		case "toRight":
			initial = 0.9;
			finall = 0.1;
			break;
		case "toLeft":
			initial = 0.1;
			finall = 0.9;
			break;
		default:
			break;
		}
		Double ScreenWidthStart = dimension.getWidth() * initial;
		int scrollStart = ScreenWidthStart.intValue();
		Double ScreenWidthEnd = dimension.getWidth() * finall;
		int scrollEnd = ScreenWidthEnd.intValue();
		new TouchAction((PerformsTouchActions) driver).press(PointOption.point(scrollStart, Anchor))
				.waitAction(WaitOptions.waitOptions(Duration.ofMillis(300)))
				.moveTo(PointOption.point(scrollEnd, Anchor)).release().perform();
		Thread.sleep(1000);
	}
}
